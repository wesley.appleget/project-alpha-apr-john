from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
from django.forms import ModelForm
from .models import Task
from django.views.generic import ListView


class TaskForm(ModelForm):
    class Meta:
        model = Task
        exclude = ["is_completed"]


@login_required
def create_task(request):
    if request.method == "POST":
        form = TaskForm(request.POST)
        if form.is_valid():
            task = form.save(commit=False)
            task.owner = request.user
            task.save()
            return redirect("project_list")
    else:
        form = TaskForm()
    context = {"form": form}
    return render(request, "create_task.html", context)


@login_required
class MyTasksListView(ListView):
    model = Task
    template_name = "tasks/my_tasks.html"
    context_object_name = "tasks"

    def get_queryset(self):
        return Task.objects.filter(assignee=self.request.user)
