from django.urls import path, include
from .views import create_task
from .views import MyTasksListView


app_name = "tasks"

urlpatterns = [
    path("mine/", MyTasksListView.as_view(), name="show_my_tasks"),
    path("create/", create_task, name="create_task"),
    path("tasks/", include("tasks.urls")),
]
